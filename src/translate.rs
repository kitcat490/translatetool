/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
use hyper::Client;
use hyper::net::HttpsConnector;
use hyper_native_tls::NativeTlsClient;
use std::time::SystemTime;
use std::io::prelude::*;
use std::fs::File;
use serde_json;
use clap::ArgMatches;
use translation_array_data::{TranslationArray, translationarray_from_file};

pub struct MicrosoftAuthKey {
    pub key: String,
    pub time: SystemTime,
}

pub fn get_microsoft_auth_token(api_key: &str) -> MicrosoftAuthKey {
    let ssl = NativeTlsClient::new().unwrap();
    let connector = HttpsConnector::new(ssl);
    let client = Client::with_connector(connector);

    let request_url: String = ["https://api.cognitive.microsoft.com/sts/v1.0/issueToken?Subscription-Key=", api_key].concat();

    let mut response = client.post(&request_url)
        .send()
        .unwrap();
    let mut auth_key_text = String::new();
    &response.read_to_string(&mut auth_key_text);

    MicrosoftAuthKey { key: auth_key_text, time: SystemTime::now() }
}

pub fn translate(arguments: ArgMatches) {
    let filename = arguments.value_of("input").unwrap();
    let translations: TranslationArray = translationarray_from_file(&filename);

    let subcommand_args: &ArgMatches = match arguments.subcommand_matches("translate") {
        None => panic!(),
        Some(x) => x,
    };

    let keyfile_name = subcommand_args.value_of("keyfile").unwrap();
    let outputlang = subcommand_args.value_of("Output Language").unwrap();

    let mut api_key = String::new();
    File::open(&keyfile_name).unwrap().read_to_string(&mut api_key).unwrap();
    let auth_token = get_microsoft_auth_token(&api_key);

    let translated_translations: TranslationArray = translations.translate(&outputlang, auth_token);
    let filename = [&outputlang, ".json"].concat();
    let mut outfile = File::create(&filename).unwrap();
    serde_json::to_writer(&mut outfile, &translated_translations).unwrap();
    println!("{:?}", translated_translations);
}
