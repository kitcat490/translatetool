/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
use std::collections::BTreeMap;
use std::io::Read;
use std::fs::File;
use translate::MicrosoftAuthKey;
use hyper::Client;
use hyper::Url;
use hyper::net::HttpsConnector;
use hyper_native_tls::NativeTlsClient;
use serde_json;
use serde_xml;

pub fn translationarray_from_file(filename: &str) -> TranslationArray {
    let mut json_string: String = String::new();
    File::open(&filename).unwrap().read_to_string(&mut json_string).unwrap();
    let translations: TranslationArray = serde_json::from_str(&json_string).unwrap();
    translations
}
impl TranslationArray {
    pub fn to_btreemap(&self) -> BTreeMap<String, String> {
        let mut btreemap = BTreeMap::new();

        for item in &self.items {
            btreemap.insert(item.key.clone(), item.value.clone());
        }
        btreemap
    }

    pub fn new() -> TranslationArray {
        let vec: Vec<TranslationItem> = Vec::new();
        TranslationArray{ items: vec }
    }

    pub fn translate(&self, lang: &str, auth_token: MicrosoftAuthKey) -> TranslationArray {
        let ssl = NativeTlsClient::new().unwrap();
        let connector = HttpsConnector::new(ssl);
        let client = Client::with_connector(connector);

        let mut translated_array: TranslationArray = TranslationArray::new();

        for item in self.items.clone() {

            if !item.key.is_empty() {
                if !item.value.is_empty() {
                    let request_url_base: String = ["https://api.microsofttranslator.com/v2/http.svc/Translate",
                                               "?appid=", "Bearer ", &auth_token.key].concat();
                    let mut request_url: Url = Url::parse(&request_url_base).unwrap();
                    request_url.query_pairs_mut().append_pair("text", &item.value);
                    request_url.query_pairs_mut().append_pair("to", lang);
                    let mut response = client.get(request_url.clone()).send().unwrap();
                    let mut response_string = String::new();
                    &response.read_to_string(&mut response_string);
                    let translation_string: String = serde_xml::from_str(&response_string).unwrap();
                    println!("{}", &translation_string);
                    translated_array.items.push(TranslationItem{key: item.key, value: translation_string});
                } else {

                }
            }
        }
        translated_array
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct TranslationArray {
    pub items: Vec<TranslationItem>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct TranslationItem {
    pub key: String,
    pub value: String,
}
