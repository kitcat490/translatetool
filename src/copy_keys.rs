/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
use std::io::prelude::*;
use std::fs::File;
use serde_json;
use clap::ArgMatches;
use translation_array_data::{TranslationArray, TranslationItem, translationarray_from_file};

pub fn copy_keys(arguments: ArgMatches) {
    let subcommand_args: &ArgMatches = match arguments.subcommand_matches("copy-keys") {
        None => panic!(),
        Some(x) => x,
    };
    let outfile_name = subcommand_args.value_of("outfile").unwrap();
    let translation_in = translationarray_from_file(arguments.value_of("input").unwrap());

    let mut translation_out = TranslationArray::new();

    for item in translation_in.items.clone() {
        translation_out.items.push(TranslationItem{key: item.key, value: "".to_string()})
    }
    let mut outfile = File::create(&outfile_name).unwrap();

    serde_json::to_writer(&mut outfile, &translation_out);
}
