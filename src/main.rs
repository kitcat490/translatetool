/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
extern crate clap;
extern crate serde;
extern crate serde_json;
extern crate hyper;
extern crate hyper_native_tls;
extern crate serde_xml;
#[macro_use] extern crate serde_derive;

mod print;
mod translate;
mod translation_array_data;
mod copy_keys;

use clap::{Arg, App, SubCommand};
use print::print_translationarray;
use translate::translate;
use copy_keys::copy_keys;

fn main() {
    let available_languages: [&str; 2] = ["en", "fi"];

        let arguments = App::new("Antidote Translation Tool")
            .version("1.0")
            .author("Michael Niehoff <kitcat490@gmail.com>")
            .about("Translates antidote json localization files using microsoft translate api")
        .subcommand(SubCommand::with_name("translate")
                    .arg(Arg::with_name("keyfile")
                         .help("Keyfile location for microsoft translate api")
                         .default_value("keyfile")
                         .long("keyfile")
                         .takes_value(true))
                    .arg(Arg::with_name("Input Language")
                         .long("inputlang")
                         .takes_value(true)
                         .default_value("en")
                         .possible_values(&available_languages))
                    .arg(Arg::with_name("Output Language")
                         .long("outputlang")
                         .takes_value(true)
                         .required(true)
                         .possible_values(&available_languages)))
        .subcommand(SubCommand::with_name("copy-keys")
                    .arg(Arg::with_name("outfile")
                         .long("outfile")
                         .takes_value(true)
                         .required(true)))
        .subcommand(SubCommand::with_name("clean")
                    .arg(Arg::with_name("Remove Empty")
                         .long("remove-empty")
                    ))
        .subcommand(SubCommand::with_name("print"))
        .arg(Arg::with_name("input")
             .help("Input json file")
             .required(true)
             .index(1))
        .get_matches();

    let mut subcommand = String::new();
    if let Some(x) = arguments.subcommand_name() {
        subcommand = x.to_string();
    } else {
        println!("Subcommand required");
    }

    match subcommand.as_str() {
        "print" => print_translationarray(arguments.value_of("input").unwrap()),
        "translate" => translate(arguments),
        "copy-keys" => copy_keys(arguments),
        _ => println!("Invalid subcommand"),
    }

}
