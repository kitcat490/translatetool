/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */
use translation_array_data::{TranslationArray};
use std::io::prelude::*;
use std::fs::File;
use serde_json;

pub fn print_translationarray(filename: &str) {
    let mut json_string: String = String::new();
    File::open(&filename).unwrap().read_to_string(&mut json_string).unwrap();
    let json: TranslationArray = serde_json::from_str(&json_string).unwrap();

    for item in &json.items {
        println!("{}: {}", item.key, item.value);
    }

    println!("Total items: {}", &json.items.len())
}
